package com.example.demo.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	// 投稿内容表⽰画⾯
	@GetMapping
	public ModelAndView top(@RequestParam(name="start", required = false) String start,@RequestParam(name="end", required = false) String end) throws ParseException {
		ModelAndView mav = new ModelAndView();
		Comment commentForm = new Comment();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport(start, end);
		// 返信を全件取得
		List<Comment> commentData = commentService.findAllComment();
		// 画⾯遷移先を指定
		mav.setViewName("/top");
		// 日付をmavにセット
		mav.addObject("start", start);
		mav.addObject("end", end);
		// DBから取得してきたデータオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		mav.addObject("formModel", commentForm);
		return mav;
	}

	// 新規投稿画⾯
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form⽤の空のentityを準備
		Report report = new Report();
		// 画⾯遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@PostMapping("/delete")
	public ModelAndView deleteContent(@RequestParam Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	// 編集画面表示
	@GetMapping("/edit")
	public ModelAndView editContent(@RequestParam Integer id) {
		ModelAndView mav = new ModelAndView();
		Report editData = reportService.findReport(id);
		mav.setViewName("/edit");
		mav.addObject("formModel", editData);
		return mav;
	}

	// 投稿更新処理
	@PostMapping("/update")
	public ModelAndView updateContent(@ModelAttribute("formModel") Report report) {
		report.setUpdatedDate(new Date());
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	// コメント登録処理
	@PostMapping("/comment")
	public ModelAndView addComment(@RequestParam(name="content") String content,@RequestParam(name="reportId") int reportId, @RequestParam(name="reportContent") String reportContent) {
		Comment comment = new Comment();
		// 投稿の更新日時の更新処理のため、reportオブジェクトを宣言
		Report report = new Report();

		comment.setContent(content);
		comment.setReportId(reportId);
		report.setId(reportId);
		report.setContent(reportContent);
		report.setUpdatedDate(new Date());

		commentService.saveComment(comment);
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	// コメント編集画面表示
	@GetMapping("/commentEdit")
	public ModelAndView CommentEditContent(@RequestParam Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment editData = commentService.findComment(id);
		mav.setViewName("/commentEdit");
		mav.addObject("formModel", editData);
		return mav;
	}

	// コメント更新処理
	@PostMapping("/commentUpdate")
	public ModelAndView CommentupdateContent(@ModelAttribute("formModel") Comment comment) {
		comment.setUpdatedDate(new Date());
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/");
	}

	// コメント削除処理
	@PostMapping("/commentDelete")
	public ModelAndView CommentDeleteContent(@RequestParam Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

}
