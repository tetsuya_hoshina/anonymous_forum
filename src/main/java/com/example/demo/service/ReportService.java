package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport(String start, String end) throws ParseException {
		// 絞り込み日時の初期化
		final String START_DATE_TIME = "2020-01-01 00:00:00";
		Date currentDateTime = new Date();
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String startTime = null;
		Date startDateTime = null;
        if(!StringUtils.isEmpty(start)) {
        	startTime = start + " 00:00:00";
        	startDateTime = sdFormat.parse(startTime);
        }else {
        	startDateTime = sdFormat.parse(START_DATE_TIME);
        }

        String endTime = null;
        Date endDateTime = null;
        if(!StringUtils.isEmpty(end)) {
        	endTime = end + " 23:59:59";
        	endDateTime = sdFormat.parse(endTime);
        }else {
        	endDateTime = currentDateTime;
        }

		// findByの引数に絞り込み日時の開始日時と終了日時をセット
		return reportRepository.findByCreatedDateBetweenOrderByUpdatedDateDesc(startDateTime, endDateTime);
	}

	// レコード追加 saveメソッドのため、登録と更新を行える
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// 編集対象レコード取得
	public Report findReport(Integer id) {
		return reportRepository.getById(id);
	}

}
