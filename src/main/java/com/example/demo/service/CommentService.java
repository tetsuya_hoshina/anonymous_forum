package com.example.demo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {

	@Autowired
	CommentRepository commentRepository;

	// 返信全件取得
		public List<Comment> findAllComment() {
			// findAllの引数にidでソートするための記述を追記
			return commentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
		}

	// 返信登録及び更新
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	// 編集対象コメント取得
	public Comment findComment(Integer id) {
		return commentRepository.getById(id);
	}

	// レコード削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}
